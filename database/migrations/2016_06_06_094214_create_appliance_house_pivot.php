<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplianceHousePivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('house_appliance')) {
            Schema::create('house_appliance', function (Blueprint $table) {
                $table->integer('house_id')->unsigned()->index();
                $table->foreign('house_id')->references('id')->on('houses')->onDelete('cascade');
                $table->integer('appliance_id')->unsigned()->index();
                $table->foreign('appliance_id')->references('id')->on('appliances')->onDelete('cascade');
                $table->primary(['house_id', 'appliance_id']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('house_appliance');
    }
}
