<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplianceMeasurePivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('appliance_measure')) {
            Schema::create('appliance_measure', function (Blueprint $table) {
                $table->integer('measure_id')->unsigned()->index();
                $table->foreign('measure_id')->references('id')->on('measures')->onDelete('cascade');
                $table->integer('appliance_id')->unsigned()->index();
                $table->foreign('appliance_id')->references('id')->on('appliances')->onDelete('cascade');
                $table->primary(['measure_id', 'appliance_id']);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('appliance_measure');
    }
}
