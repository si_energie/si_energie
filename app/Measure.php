<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Measure extends Model
{
    // -------------------------------------------------------------------------------
    // Colonnes protégées
    // -------------------------------------------------------------------------------
    protected $guarded = ['id'];
    protected $dates = ['date', 'created_at', 'updated_at'];

    // -------------------------------------------------------------------------------
    // Initialisation du nom de la table
    // -------------------------------------------------------------------------------
    protected $table = 'measures';

    public function appliance(){
        return $this->belongsToMany(Appliance::class, 'appliance_measure');
    }

}
