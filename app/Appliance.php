<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appliance extends Model
{
    // -------------------------------------------------------------------------------
    // Colonnes protégées
    // -------------------------------------------------------------------------------
    protected $guarded = ['id'];

    // -------------------------------------------------------------------------------
    // Initialisation du nom de la table
    // -------------------------------------------------------------------------------
    protected $table = 'appliances';

    public function house()
    {
        return $this->belongsToMany(House::class, 'house_appliance');
    }

    public function measures(){
        return $this->belongsToMany(Measure::class, 'appliance_measure');
    }

}
