<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    // -------------------------------------------------------------------------------
    // Colonnes protégées
    // -------------------------------------------------------------------------------
    protected $guarded = ['id'];

    // -------------------------------------------------------------------------------
    // Initialisation du nom de la table
    // -------------------------------------------------------------------------------
    protected $table = 'houses';

    public function appliances(){
        return $this->belongsToMany(Appliance::class, 'house_appliance');
    }

}
