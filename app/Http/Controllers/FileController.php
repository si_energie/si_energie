<?php
/**
 * Created by PhpStorm.
 * User: Pierre-Antoine
 * Date: 02/03/2016
 * Time: 09:52
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;

class FileController extends Controller
{
    public function printTotal(){
        $zippedFileSystem = new \FilesystemIterator(public_path() . '/files/zip/data', \FilesystemIterator::SKIP_DOTS);
        $unzippedFileSystem = new \FilesystemIterator(public_path() . '/files/unzip/Enertech/Campagnes/Remodece/Travail/Files', \FilesystemIterator::SKIP_DOTS);
        $totalZ = iterator_count($zippedFileSystem);
        $totalUZ = iterator_count($unzippedFileSystem);
        dd( [
            'zips'=>$totalZ,
            'unzips'=>$totalUZ,
            'decompression_rate'=>(($totalUZ == 0 ? 100 :$totalUZ * 100 / $totalZ))
        ]);
    }

    public function upload(Request $request){
        dd($request->attributes);
    }

    public function unzip(){
        ini_set('memory_limit', -1);
        set_time_limit(0);
        $time_start_unzip_all = microtime(true);
        $zipper = new \Chumper\Zipper\Zipper;
        $zipper->make('files/data_consommations_domestiques_completes.zip')->extractTo('files/zip');
        $time_end_unzip_all = microtime(true);
        $time_unzip_all = $time_end_unzip_all - $time_start_unzip_all;
        $time_start_extract_file = microtime(true);
        $files = glob('files/zip/data/*');
        foreach ($files as $file) {
            $zipper->make($file)->extractTo('files/unzip');
        }
        $time_end_extract = microtime(true);
        $time_extract = $time_end_extract - $time_start_extract_file;

        $time_parse = microtime(true);
        $files = glob('files/unzip/Enertech/Campagnes/Remodece/Travail/Files/*');
        foreach ($files as $key => $file) {
            try {
                DB::beginTransaction();
                // database queries here

                $currentFile = new \App\FileZ();
                $currentFile->name = $file;


                $lines = file($file, FILE_IGNORE_NEW_LINES);
                $nb_null = 0;
                $total = 0;

                foreach ($lines as $keyLine => $line) {

                    switch ($keyLine) {
                        //project
                        case 0:
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            break;
                        default:
                            $total++;
                            $lineValues = explode("\t", $line);
                            if (intval($lineValues[3]) == 0) {
                                $nb_null++;
                            }
                            break;

                    }

                }
                $percent_null = 0;
                if ($total == 0 || $nb_null == 0) {
                    $percent_null = $nb_null;
                } else {
                    $percent_null = $nb_null * 100 / $total;
                }
                    $currentFile->percent_null = intval($percent_null);
                $currentFile->save();
                DB::commit();
            } catch (\PDOException $e) {
                // Woopsy
                DB::rollBack();
            }
        }
        $time_end_parse = microtime(true);
        $time_parse = $time_end_parse - $time_parse;

        $time_total = $time_end_parse - $time_start_unzip_all;
        return [
            'extract_archive_time' => $time_unzip_all,
            'extract_files_time' => $time_extract,
            'parse_files_time' => $time_parse,
            'total_process_time' => $time_total
        ];
    }

    public function clear(){
        $filesZipped = $files = glob('files/zip/data/*');
        $filesUnzipped = $files = glob('files/unzip/Enertech/Campagnes/Remodece/Travail/Files/*');
        $nbDelete = 0;

        foreach ($filesZipped as $file) { // iterate files
            if (is_file($file)) {
                unlink($file); // delete file
                $nbDelete++;
            }
        }
        foreach ($filesUnzipped as $file) { // iterate files
            if (is_file($file)) {
                unlink($file); // delete file
                $nbDelete++;
            }
        }
        dd($nbDelete . ' files deleted');
    }

    public function truncate(){
        $total = 0;
        $total += sizeof(DB::table('houses')->get());
        $total += sizeof(DB::table('appliances')->get());
        $total += sizeof(DB::table('appliance_measure')->get());
        $total += sizeof(DB::table('house_appliance')->get());
        $total += sizeof(DB::table('measures')->get());
        //$total += sizeof(DB::table('files')->get());

        DB::table('houses')->delete();
        DB::table('appliances')->delete();
        DB::table('appliance_measure')->delete();
        DB::table('house_appliance')->delete();
        DB::table('measures')->delete();
        DB::table('files')->delete();

        dd($total . ' records deleted');
    }

}