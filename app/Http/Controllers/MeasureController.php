<?php
/**
 * Created by PhpStorm.
 * User: Pierre-Antoine
 * Date: 02/03/2016
 * Time: 09:52
 */

namespace App\Http\Controllers;

use App\Appliance;
use App\FileZ;
use App\House;
use App\Measure;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class MeasureController extends Controller
{
    public function hard($nbFiles, $pMin, $pMax)
    {

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        ini_set('memory_limit', -1);
        set_time_limit(0);
        $time_start = microtime(true);
        $nbTuples = 0;
        $totalLines = 0;

        $filesPerPercent = FileZ::whereBetween('percent_null', [$pMin, $pMax])->get();
        $files = $filesPerPercent->random($nbFiles);
        $files->all();

        if ($nbFiles == 1) {
            $tmp = collect();
            $tmp->push($files);
            $files = $tmp;
        }

        try {
            DB::beginTransaction();
            // database queries here

            foreach ($files as $key => $file) {
                $lines = file($file->name, FILE_IGNORE_NEW_LINES);
                $house = null;
                $applianceO = null;
                foreach ($lines as $keyLine => $line) {
                    $totalLines++;
                    switch ($keyLine) {
                        //project
                        case 0:
                            $project = explode(" : ", $line)[1];
                            break;
                        //household
                        case 1:
                            $houseHold = explode(" : ", $line)[1];
                            $house = House::firstOrCreate(['household' => $houseHold]);
                            if (!$house->exists) {
                                $nbTuples++;
                            }
                            break;
                        //appliance
                        case 2:
                            $lineParsed = explode(" : ", $line)[1];
                            $datasFromLines = explode("(", $lineParsed);
                            $appliance = trim($datasFromLines[0]);
                            $infos = (substr($datasFromLines[1], 0, -1));
                            $applianceO = Appliance::create([
                                'name' => $appliance,
                                'complementary' => $infos
                            ]);
                            $nbTuples++;
                            $house->appliances()->attach($applianceO);
                            $nbTuples++;
                            break;
                        //nothing
                        case 3:
                        case 4:
                            break;
                        //Values
                        default:
                            $lineValues = explode("\t", $line);
                            $date = Carbon::createFromFormat('d/m/y H:i', $lineValues[0] . ' ' . $lineValues[1]);
                            $measure = Measure::create([
                                'date' => $date->format('Y-m-d H:i:s'),
                                'value' => $lineValues[3]
                            ]);
                            $nbTuples++;
                            $applianceO->measures()->attach($measure);
                            $nbTuples++;
                            break;
                    }
                }
            }
            DB::commit();
        } catch (\PDOException $e) {
            // Woopsy
            DB::rollBack();
        }
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        dd([
                'seconds' => $time,
                'total_lines' => $totalLines,
                'records' => $nbTuples,
                'files' => $files]
        );
    }

    public function withoutNullValues($nbFiles, $pMin, $pMax)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        ini_set('memory_limit', -1);
        set_time_limit(0);
        $time_start = microtime(true);
        $nullValuesSkipped = 0;
        $nbTuples = 0;
        $totalLines = 0;

        $filesPerPercent = FileZ::whereBetween('percent_null', [$pMin, $pMax])->get();
        $files = $filesPerPercent->random($nbFiles);
        $files->all();
        if ($nbFiles == 1) {
            $tmp = collect();
            $tmp->push($files);
            $files = $tmp;
        }

        try {
            DB::beginTransaction();
            // database queries here
            foreach ($files as $key => $file) {
                $lines = file($file->name, FILE_IGNORE_NEW_LINES);
                $house = null;
                $applianceO = null;
                foreach ($lines as $keyLine => $line) {

                    $totalLines++;
                    switch ($keyLine) {
                        //project
                        case 0:
                            $project = explode(" : ", $line)[1];
                            break;
                        //household
                        case 1:
                            $houseHold = explode(" : ", $line)[1];
                            $house = House::firstOrCreate(['household' => $houseHold]);
                            if (!$house->exists) {
                                $nbTuples++;
                            }
                            break;
                        //appliance
                        case 2:
                            $lineParsed = explode(" : ", $line)[1];
                            $datasFromLines = explode("(", $lineParsed);
                            $appliance = trim($datasFromLines[0]);
                            $infos = (substr($datasFromLines[1], 0, -1));
                            $applianceO = Appliance::create([
                                'name' => $appliance,
                                'complementary' => $infos
                            ]);
                            $nbTuples++;
                            $house->appliances()->attach($applianceO);
                            $nbTuples++;
                            break;
                        //nothing
                        case 3:
                        case 4:
                            break;
                        //Values
                        default:
                            $lineValues = explode("\t", $line);
                            if (intval($lineValues[3]) != 0) {
                                $date = Carbon::createFromFormat('d/m/y H:i', $lineValues[0] . ' ' . $lineValues[1]);
                                $measure = Measure::create([
                                    'date' => $date->format('Y-m-d H:i:s'),
                                    'value' => $lineValues[3]
                                ]);

                                $nbTuples++;
                                $applianceO->measures()->attach($measure);
                                $nbTuples++;
                            } else {
                                $nullValuesSkipped += 2;
                            }
                            break;
                    }
                }
            }
            DB::commit();
        } catch (\PDOException $e) {
            // Woopsy
            DB::rollBack();
        }
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        dd([
                'seconds' => $time,
                'total_lines' => $totalLines,
                'records' => $nbTuples,
                'null_skipped' => $nullValuesSkipped,
                'files' => $files
            ]
        );
    }

    public function timelapse($nbFiles, $pMin, $pMax)
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        ini_set('memory_limit', -1);
        set_time_limit(0);

        $time_start = microtime(true);

        $nullValuesSkipped = 0;
        $nbTuples = 0;
        $totalSkipped = 0;
        $totalLines = 0;

        $filesPerPercent = FileZ::whereBetween('percent_null', [$pMin, $pMax])->get();
        $files = $filesPerPercent->random($nbFiles);
        $files->all();
        if ($nbFiles == 1) {
            $tmp = collect();
            $tmp->push($files);
            $files = $tmp;
        }

        try {
            DB::beginTransaction();
            // database queries here
            foreach ($files as $key => $file) {
                $lines = file($file->name, FILE_IGNORE_NEW_LINES);
                $house = null;
                $applianceO = null;
                $indexLine = 0;
                $sumValue = 0;
                foreach ($lines as $keyLine => $line) {
                    $totalLines++;

                    switch ($keyLine) {
                        //project
                        case 0:
                            $project = explode(" : ", $line)[1];
                            break;
                        //household
                        case 1:
                            $houseHold = explode(" : ", $line)[1];
                            $house = House::firstOrCreate(['household' => $houseHold]);
                            if (!$house->exists) {
                                $nbTuples++;
                            }
                            break;
                        //appliance
                        case 2:
                            $lineParsed = explode(" : ", $line)[1];
                            $datasFromLines = explode("(", $lineParsed);
                            $appliance = trim($datasFromLines[0]);
                            $infos = (substr($datasFromLines[1], 0, -1));
                            $applianceO = Appliance::create([
                                'name' => $appliance,
                                'complementary' => $infos
                            ]);
                            $nbTuples++;
                            $house->appliances()->attach($applianceO);
                            $nbTuples++;
                            break;
                        //nothing
                        case 3:
                        case 4:
                            break;
                        //Values
                        default:
                            $indexLine++;

                            $lineValues = explode("\t", $line);

                            $sumValue += $lineValues[3];

                            if ($indexLine == 6) {
                                $date = Carbon::createFromFormat('d/m/y H:i', $lineValues[0] . ' ' . $lineValues[1]);
                                $measure = Measure::create([
                                    'date' => $date->format('Y-m-d H:i:s'),
                                    'value' => $sumValue / 6
                                ]);
                                $nbTuples++;
                                $applianceO->measures()->attach($measure);
                                $nbTuples++;
                                $indexLine = 0;
                                $sumValue = 0;
                            }

                            break;
                    }

                }
            }

            DB::commit();
        } catch (\PDOException $e) {
            // Woopsy
            DB::rollBack();
        }
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        dd([
                'seconds' => $time,
                'total_lines' => $totalLines,
                'records' => $nbTuples,
                'null_skipped' => $nullValuesSkipped,
                'range_skipped' => $totalSkipped,
                'files' => $files
            ]
        );

    }

    //consso par jour / appliance X pour maison X
    public function applianceConssomation($houseHold, $appliance)
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        $time_start = microtime(true);
        $house = House::where('household', $houseHold)->first();
        if (!is_null($house)) {
            $appliance = $house->appliances->where('name', $appliance);
            if (!is_null($appliance)) {
                $houseMeasures = $appliance[0]->measures;
                $sorted = $houseMeasures->sortByDesc('date');
                $res = $sorted->values()->all();
                $mostRecentMeasure = $res[0];
                $t = $mostRecentMeasure->date->copy()->subDay();

                $filteredCollection = $sorted->filter(function ($item) use ($t, $mostRecentMeasure) {
                    return ($item->date->between($t, $mostRecentMeasure->date));
                })->values();
                //Get for day
                $conssoOnLastDay = ($filteredCollection->sum('value'));

                $time_end = microtime(true);
                $time = $time_end - $time_start;

                return [
                    'total_time' => $time,
                    'consso' => $conssoOnLastDay
                ];
            }
        }

    }

    //OK = CONSSOMATION APPAREILS POUR UNE MAISON POUR UNE HEURE
    public function houseConssomation($houseHold)
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        $time_start = microtime(true);
        $house = House::where('household', $houseHold)->first();
        if (!is_null($house)) {
            //Récupération de l'heure de la dernière mesure connu pour la maison.
            $houseMeasures = collect();
            foreach ($house->appliances as $appliance) {
                foreach ($appliance->measures as $measure) {
                    $houseMeasures->push($measure);
                }
            }

            $sorted = $houseMeasures->sortByDesc('date');
            $res = $sorted->values()->all();

            $mostRecentMeasure = $res[0];
            $t = $mostRecentMeasure->date->copy()->subHour();

            $filteredCollection = $sorted->filter(function ($item) use ($t, $mostRecentMeasure) {
                return ($item->date->between($t, $mostRecentMeasure->date));
            })->values();

            /*   foreach($filteredCollection as $filterC){
                   dump($filterC->value);
                   dump($filterC->date);
               }*/
            $conssoOnLastHour = ($filteredCollection->sum('value'));

            $time_end = microtime(true);
            $time = $time_end - $time_start;

            return [
                'total_time' => $time,
                'consso' => $conssoOnLastHour
            ];

        }
    }

//¥	“l’appliance” x qui a consommé le plus lors du dernier mois.
    public function needyApplianceOnLastMonth($appliance)
    {
        ini_set('memory_limit', -1);
        set_time_limit(0);

        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        $time_start = microtime(true);
        $houses = House::all();
        if (!is_null($houses)) {
            //Récupération de l'heure de la dernière mesure connu pour la maison.
            $houseAppliancesMeasures = collect();
            $measures = collect();
            foreach ($houses as $house) {
                foreach ($house->appliances->where('name', $appliance) as $app) {
                    //dump($app->house); OK
                    $houseAppliancesMeasures->put($house->household, collect());
                    foreach ($app->measures as $measure) {
                        $houseAppliancesMeasures->get($house->household)->push($measure);
                        $measures->push($measure);
                    }
                }
            }
            $sorted = $measures->sortByDesc('date');
            $res = $sorted->values()->all();
            $mostRecentMeasure = $res[0];
            $t = $mostRecentMeasure->date->copy()->subMonth();
            //dump($t);
            //dump($mostRecentMeasure->date);
            $filteredCollection = collect();
            foreach($houseAppliancesMeasures as $key => $houseM){
                $res = $houseM->filter(function ($item) use ($t, $mostRecentMeasure) {
                    return ($item->date->between($t, $mostRecentMeasure->date));
                })->values();
                $filteredCollection->put($key,$res );
            }

            $conssoOnLastMonth = [
                'household'=>null,
                'appliance'=>$mostRecentMeasure->appliance[0]->name,
                'consso'=>0,
                'total_time'=>0,
            ];

            foreach($filteredCollection as $key => $house){
                $conssoOnLastMonthForCurrent = ($house->sum('value'));
                if($conssoOnLastMonthForCurrent>$conssoOnLastMonth['consso']){
                    $conssoOnLastMonth['household'] = $key;
                    $conssoOnLastMonth['consso'] = $conssoOnLastMonthForCurrent;
                }
            }
            $time_end = microtime(true);
            $time = $time_end - $time_start;
            $conssoOnLastMonth['total_time'] = $time;

            return $conssoOnLastMonth;

        }


        /*     //Gest last mont
             DB::statement('SET FOREIGN_KEY_CHECKS = 0');
             $time_start = microtime(true);
             $appliances = Appliance::where('name', $appliance)->get();
             if (!is_null($appliances)) {

                 $appliancesMeasures = collect();
                 $measures = collect();
                 foreach ($appliances as $appliance) {
                     foreach ($appliance->measures as $measure) {
                         $appliancesMeasures->put($appliance->name, $measure);
                         $measures->push($measure);
                     }
                 }
                 //Get last month measure
                 $sorted = $measures->sortByDesc('date');
                 $res = $sorted->values()->all();

                 $mostRecentMeasure = $res[0];
                 $t = $mostRecentMeasure->date->copy()->subMonth();

                 dump($filteredCollection);
                 $filteredCollection = $appliancesMeasures->filter(function ($item) use ($t, $mostRecentMeasure) {
                     dd($item);
                     return ($item->date->between($t, $mostRecentMeasure->date));
                 })->values();


             }*/

    }

    public function periodsGreaterThanLastWeek()
    {

    }


}


/*
 *
 DELETE FROM appliances;
DELETE FROM appliance_measure;
DELETE FROM houses;
DELETE FROM house_appliance;
DELETE FROM measures;
 * */
