<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'files'], function () {
    Route::get('clear', 'FileController@clear');
    Route::get('unzip_files', 'FileController@unzip');
});

Route::group(['prefix' => 'decompress/{nbfile?}/{percent_n_min?}/{percent_n_max?}'], function () {
    Route::get('/brut', 'MeasureController@hard');
    Route::get('/withoutnull', 'MeasureController@withoutNullValues');
    Route::get('/timelapse', 'MeasureController@timelapse');
});

Route::get('total', 'FileController@printTotal');
Route::post('apply/upload', 'FileController@upload');
Route::get('upload', function () {
    return View::make('pages.upload');
});

Route::get('/truncate', 'FileController@truncate');

Route::group(['prefix' => 'request'], function () {
    //conssomation de l'appliance X d'une maison Y sur la journée
    Route::get('/appliance/{household?}/{appliance?}', 'MeasureController@applianceConssomation');
    //Conssomation d'une maison X à la dernière heure
    Route::get('/house/{household?}', 'MeasureController@houseConssomation');
    //l’appliance” x qui a consommé le plus lors du dernier mois.
    Route::get('/needyApplianceOnLastMonth/{appliance?}', 'MeasureController@needyApplianceOnLastMonth');
    //Sur la dernière semaine, les périodes de temps où la consommation est plus élevée que la semaine précédente.
    Route::get('/appliance/periods', 'MeasureController@periodsGreaterThanLastWeek');

});

Route::get('files/histo', function () {
    $res = [
        "0-10" => 0,
        "10-20" => 0,
        "20-30" => 0,
        "30-40" => 0,
        "40-50" => 0,
        "50-60" => 0,
        "60-70" => 0,
        "70-80" => 0,
        "80-90" => 0,
        "90-100" => 0
    ];
    foreach (\App\FileZ::all() as $file) {
        $p = $file->percent_null;
        if ($p >= 0 && $p < 10) {
            $res["0-10"] += $p;
        } else if ($p >= 10 && $p < 20) {
            $res["10-20"] += $p;
        } else if ($p >= 20 && $p < 30) {
            $res["20-30"] += $p;
        } else if ($p >= 30 && $p < 40) {
            $res["30-40"] += $p;
        } else if ($p >= 40 && $p < 50) {
            $res["40-50"] += $p;
        } else if ($p >= 50 && $p < 60) {
            $res["50-60"] += $p;
        } else if ($p >= 60 && $p < 70) {
            $res["60-70"] += $p;
        } else if ($p >= 70 && $p < 80) {
            $res["70-80"] += $p;
        } else if ($p >= 80 && $p < 90) {
            $res["80-90"] += $p;
        } else if ($p >= 90 && $p < 100) {
            $res["90-100"] += $p;
        }

    }
    dd($res);
});

Route::get('measures/average/hour', function () {
    ini_set('memory_limit', -1);
    set_time_limit(0);
    $res = [
        "0_1" => 0,
        "1_2" => 0,
        "2_3" => 0,
        "3_4" => 0,
        "4_5" => 0,
        "5_6" => 0,
        "6_7" => 0,
        "7_8" => 0,
        "8_9" => 0,
        "9_10" => 0,
        "10_11" => 0,
        "11_12" => 0,
        "12_13" => 0,
        "13_14" => 0,
        "14_15" => 0,
        "15_16" => 0,
        "16_17" => 0,
        "17_18" => 0,
        "18_19" => 0,
        "19_20" => 0,
        "20_21" => 0,
        "21_22" => 0,
        "22_23" => 0,
        "23_00" => 0
    ];
    foreach (\App\Measure::all() as $m) {
        $md = $m->date->hour;
        if ($md >= 0 && $md < 1) {
            $res["0_1"] += $m->value;
        } else if ($md >= 1 && $md < 2) {
            $res["1_2"] += $m->value;
        } else if ($md >= 2 && $md < 3) {
            $res["2_3"] += $m->value;
        } else if ($md >= 3 && $md < 4) {
            $res["3_4"] += $m->value;
        } else if ($md >= 4 && $md < 5) {
            $res["4_5"] += $m->value;
        } else if ($md >= 5 && $md < 6) {
            $res["5_6"] += $m->value;
        } else if ($md >= 6 && $md < 7) {
            $res["6_7"] += $m->value;
        } else if ($md >= 7 && $md < 8) {
            $res["7_8"] += $m->value;
        } else if ($md >= 8 && $md < 9) {
            $res["8_9"] += $m->value;
        } else if ($md >= 9 && $md < 10) {
            $res["9_10"] += $m->value;
        } else if ($md >= 10 && $md < 11) {
            $res["10_11"] += $m->value;
        } else if ($md >= 11 && $md < 12) {
            $res["11_12"] += $m->value;
        } else if ($md >=12 && $md < 13) {
            $res["12_13"] += $m->value;
        } else if ($md >= 13 && $md < 14) {
            $res["13_14"] += $m->value;
        } else if ($md >= 14 && $md < 15) {
            $res["14_15"] += $m->value;
        } else if ($md >= 15 && $md < 16) {
            $res["15_16"] += $m->value;
        } else if ($md >= 16 && $md < 17) {
            $res["16_17"] += $m->value;
        } else if ($md >= 17 && $md < 18) {
            $res["17_18"] += $m->value;
        } else if ($md >= 18 && $md < 19) {
            $res["18_19"] += $m->value;
        } else if ($md >= 19 && $md < 20) {
            $res["19_20"] += $m->value;
        } else if ($md >= 20 && $md < 21) {
            $res["20_21"] += $m->value;
        } else if ($md >= 21 && $md < 22) {
            $res["21_22"] += $m->value;
        } else if ($md >= 22 && $md < 23) {
            $res["22_23"] += $m->value;
        } else if ($md >= 23 && $md < 24) {
            $res["23_00"] += $m->value;
        }
    }
    dd($res);
});

Route::get('measures/ranges', function () {
    ini_set('memory_limit', -1);
    set_time_limit(0);
    $res = [
        "nuit" => 0,
        "matin" => 0,
        "midi" => 0,
        "après-midi" => 0,
        "soirée" => 0
    ];
    foreach (\App\Measure::all() as $m) {
        $md = $m->date->hour;
        if ($md >= 0 && $md < 7) {
            $res["nuit"] += $m->value;
        } else if ($md >= 7 && $md < 12) {
            $res["matin"] += $m->value;
        } else if ($md >= 12 && $md < 14) {
            $res["midi"] += $m->value;
        } else if ($md >= 14 && $md < 18) {
            $res["après-midi"] += $m->value;
        } else if ($md >= 18 && $md < 24) {
            $res["soirée"] += $m->value;
        }
    }
    dd($res);
});