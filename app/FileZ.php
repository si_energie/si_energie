<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileZ extends Model
{
    // -------------------------------------------------------------------------------
    // Colonnes protégées
    // -------------------------------------------------------------------------------
    protected $guarded = ['id'];

    // -------------------------------------------------------------------------------
    // Initialisation du nom de la table
    // -------------------------------------------------------------------------------
    protected $table = 'files';
}
