<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>UNZIP SYSTEM | SIE</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description"/>
    <link href="/template/mt450/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/template/mt450/plugins/bootstrap/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/template/mt450/plugins/uniform/css/uniform.default.min.css" rel="stylesheet" type="text/css">
    <link href="/template/mt450/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css">
    <link href="/template/mt450/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/template/mt450/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css">
    <link href="/template/mt450/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet"
          type="text/css">
    <link href="/template/mt450/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">

    <link href="/template/mt450/css/components-md.css" id="style_components" rel="stylesheet" type="text/css">
    <link href="/template/mt450/css/plugins-md.css" rel="stylesheet" type="text/css">
    <link href="/template/mt450/css/animate.css" rel="stylesheet" type="text/css">
    {{--  <link href="/template/mt450/themes/theme1/layout.css" rel="stylesheet" type="text/css">
     <link href="/template/mt450/themes/theme1/default.css" rel="stylesheet" type="t
     <link href="/template/mt450/css/custom.css" rel="stylesheet" type="text/css">--}}

    <link href="/template/mt450/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet"
          type="text/css"/>
    <link href="/template/mt450/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="/template/mt450/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="/template/mt450/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="/template/mt450/plugins/clockface/css/clockface.css" rel="stylesheet" type="text/css"/>

    <link href="/template/mt450/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet"
          type="text/css"/>
    <link href="/template/mt450/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>

    <link href="/template/mt450/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css"/>
    <link href="/template/mt450/plugins/google-code-prettify/prettify.css" rel="stylesheet" type="text/css"/>
</head>

<body>

@yield('content')

<script src="/template/mt450/plugins/jquery/jquery.min.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/jquery/jquery-ui.min.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/blockui/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/jszip.js" type="text/javascript"></script>

<script src="/template/mt450/plugins/datatable.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/datatables/datatables.all.min.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js"
        type="text/javascript"></script>

<script src="/template/mt450/plugins/sweetalert/sweetalert.min.js" type="text/javascript"></script>

<script src="/template/mt450/plugins/amcharts/amcharts/amcharts.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/amcharts/amcharts/serial.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/amcharts/amcharts/pie.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/amcharts/amcharts/radar.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/amcharts/amcharts/themes/light.js" type="text/javascript"></script>

<script src="/template/mt450/plugins/amcharts/ammap/ammap.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/amcharts/ammap/maps/js/worldLow.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/amcharts/amstockcharts/amstock.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/amcharts/amcharts/plugins/dataloader/dataloader.min.js"
        type="text/javascript"></script>

<script src="/template/mt450/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"
        type="text/javascript"></script>
<script src="/template/mt450/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.fr.min.js"
        type="text/javascript"></script>

<script src="/template/mt450/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"
        type="text/javascript"></script>
<script src="/template/mt450/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"
        type="text/javascript"></script>
<script src="/template/mt450/plugins/clockface/js/clockface.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.fr.js"
        type="text/javascript"></script>

<script src="/template/mt450/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"
        type="text/javascript"></script>

<script src="/template/mt450/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/jquery-inputmasks/inputmask.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/jquery-inputmasks/jquery.inputmask.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/jquery-inputmasks/inputmask.numeric.extensions.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js" type="text/javascript"></script>

<script src="/template/mt450/app.min.js" type="text/javascript"></script>
<script src="/template/mt450/themes/theme1/layout.min.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="/template/mt450/plugins/google-code-prettify/prettify.js" type="text/javascript"></script>

@stack('scripts')
</body>

</html>